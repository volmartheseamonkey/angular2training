import { Component, OnInit } from '@angular/core';
import { TodouService } from './todou.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})

export class TodosComponent implements OnInit {
  list = [{ title: 'Buy Delorean'}];
  constructor(public todoService: TodouService) { }

  ngOnInit() {
    this.todoService.getTodos().then( response => this.list = [response]);
  }

}
