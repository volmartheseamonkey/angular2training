import { TestBed, inject } from '@angular/core/testing';

import { TodouService } from './todou.service';

describe('TodouService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodouService]
    });
  });

  it('should ...', inject([TodouService], (service: TodouService) => {
    expect(service).toBeTruthy();
  }));
});
