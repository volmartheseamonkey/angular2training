import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import "rxjs/Rx";

@Injectable()
export class TodouService {

  constructor(private http: Http) { }

  getTodos() {
	return this.http.get('//jsonplaceholder.typicode.com/posts/1')
	.map(response => response.json())
	.toPromise();
  }
}
