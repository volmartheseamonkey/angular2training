import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';
import { TodouService } from './todos/todou.service';
import { TodoDetailsComponent } from './todos/todo-details/todo-details.component';

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    TodoDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [TodouService],
  bootstrap: [AppComponent]
})
export class AppModule { }
