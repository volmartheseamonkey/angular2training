import { RouterModule, Routes } from '@angular/router';
import { TodosComponent } from './todos/todos.component';
import { TodoDetailsComponent } from './todos/todo-details/todo-details.component';

const routes: Routes = [
	{path: 'todos', component: TodoDetailsComponent }
];

export const routing = RouterModule.forRoot(routes);
